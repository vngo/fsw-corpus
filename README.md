# The French StoryWeaver (FSW) datasets

This repository provides you with the following datasets:
- The FSW dataset with documents
- The FSW dataset with sentences
- The reference dataset for author-document matching

Each of the dataset comes in two formats: `.json` and `.tsv`


## Getting started

### JSON files
The JSON files are designed to work with PolaRS library. To install the library with PIP:
`pip install polars`

Upon the successful installation of the library, the dataset can be loaded. The following snippet loads the dataset into the memory on eager mode:

```
import polars as pl

df = pl.read_json(path_to_the_json_file)

df
```
For lazy or SQL-ike modes, consult https://www.pola.rs/

### TSV files
The TSV files are to support users who prefer Pandas or libraries that support CSV/TSV files.
Please make yourself at home.

## Citation
If you use the datasets, please cite the publication below:
<to be updated>
